/*
 * @Author: zheng yong tao
 * @Date: 2022-01-15 15:23:41
 * @LastEditors: zheng yong tao
 * @LastEditTime: 2022-03-05 23:50:00
 * @Description:
 */
import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);
export default new Router({
  routes: [
    {
      path: "/",
      redirect: "/home"
    },
    {
      path: "/home",
      name: "home",
      component: resolve => require(["@/view/home"], resolve)
    },
    {
      path: "/filing",
      name: "filing",
      component: resolve => require(["@/view/filing"], resolve)
    },
    {
      path: "/about",
      name: "about",
      component: resolve => require(["@/view/about"], resolve)
    },
    {
      path: "/column",
      name: "column",
      component: resolve => require(["@/view/column"], resolve)
    },
    {
      path: "/articleDetail",
      name: "articleDetail",
      component: resolve => require(["@/view/articleDetail"], resolve)
    },
    {
      path: "/articleEdit",
      name: "articleEdit",
      component: resolve => require(["@/view/articleEdit"], resolve)
    },
    {
      path: "/articleWrite",
      name: "articleWrite",
      component: resolve => require(["@/view/articleWrite"], resolve)
    },
    {
      path: "/statistics",
      name: "statistics",
      component: resolve => require(["@/view/statistics"], resolve)
    },
    {
      path: "/audit",
      name: "audit",
      component: resolve => require(["@/view/audit"], resolve)
    },
    {
      path: "/config",
      name: "config",
      component: resolve => require(["@/view/config"], resolve)
    }
  ]
});
