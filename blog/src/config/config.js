/*
 * @Author: zheng yong tao
 * @Date: 2022-02-01 23:22:59
 * @LastEditors: zheng yong tao
 * @LastEditTime: 2022-03-06 10:54:32
 * @Description:
 */
export const config = {
  adminRoute: ["articleEdit", "articleWrite", "statistics", "audit"], //需要管理员权限的路由入口
  openRoute: ["home", "column", "filing", "about", "articleDetail"] //公开的路由入口
};
//数据库配置
export const dbConfig = {
  url: "http://localhost",
  port: "3001"
};
//秘钥配置
export const key = "jyeontu6666abcd12345";
